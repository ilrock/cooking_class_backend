class CreateGuestLists < ActiveRecord::Migration[5.1]
  def change
    create_table :guest_lists do |t|
      t.references :user
      t.references :cooking_class
      t.timestamps
    end
  end
end
