class CreateCookingClasses < ActiveRecord::Migration[5.1]
  def change
    create_table :cooking_classes do |t|
      t.integer :host_id
      t.string :title
      t.text :description
      t.monetize :price
      t.integer :min_guest
      t.integer :max_guest
      t.string :cuisine

      t.timestamps
    end
  end
end
