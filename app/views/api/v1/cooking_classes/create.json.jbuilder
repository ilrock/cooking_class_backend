json.(@cooking_class, :id, :title, :description, :cuisine, :min_guest, :max_guest, :price_cents)
json.host @cooking_class.host, :id, :name, :email
