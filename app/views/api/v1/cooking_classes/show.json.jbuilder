json.(@cooking_class, :id, :title, :description, :cuisine, :min_guest, :max_guest, :price_cents)
json.host @cooking_class.host, :id, :name, :email
json.guests @cooking_class.guests do |guest|
  json.(guest, :id, :name, :email)
end
