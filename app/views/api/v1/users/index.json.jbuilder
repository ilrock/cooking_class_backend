json.users @users do |user|
  json.(user, :id, :name, :email)

  if user.is_host
    json.events  user.organized_events do |event|
      json.(event, :id, :title, :description, :min_guest, :max_guest, :cuisine)
    end
  end
end
