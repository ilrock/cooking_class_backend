class Api::V1::CookingClassesController < ApplicationController
  before_action :set_cooking_class, only: [:show, :update, :destroy]
  before_action :authenticate_user, except: [:index, :show]

  # GET /cooking_classes
  def index
    @cooking_classes = CookingClass.all

    render :index, status: :ok
  end

  # GET /cooking_classes/1
  def show
    if @cooking_class
      render :show, status: :ok
    else
      head(:not_found)
    end
  end

  # POST /cooking_classes
  def create
    @cooking_class = CookingClass.new(cooking_class_params)
    @cooking_class.host = current_user
    @cooking_class.price_cents = params[:price].to_i * 100

    if @cooking_class.save
      render :create, status: :created
    else
      render json: @cooking_class.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /cooking_classes/1
  def update
    if @cooking_class.update(cooking_class_params)
      render :update
    else
      render json: @cooking_class.errors, status: :unprocessable_entity
    end
  end

  # DELETE /cooking_classes/1
  def destroy
    @cooking_class.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cooking_class
      @cooking_class = CookingClass.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cooking_class_params
      params.permit(:title, :description, :min_guest, :max_guest, :cuisine)
    end
end
