class User < ApplicationRecord
  has_secure_password

  has_many :organized_events, class_name: "CookingClass", foreign_key: "host_id"

  has_many :guest_lists
  has_many :attended_events, through: :guest_lists, source: :cooking_class

  def is_host
    if organized_events != []
      return true
    else
      return false
    end
  end
end
